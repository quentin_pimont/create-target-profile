const { default: knex } = require('knex');

require('dotenv').config();

const pg = require('knex')({
    client: 'pg',
    connection: process.env.DATABASE,
    searchPath: ['knex', 'public'],
});

const skills = [
'V-Boucles_NV1',
'V-Boucles_NV5',
'V-Boucles_NV6',
'V-Conditions_NV2',
'V-Conditions_NV3',
'V-Conditions_NV5',
'V-DefinitionBooleen_NV1',
'V-ExpressionsBooleennesMultiples_NV4',
'V-ExpressionsBooleennesMultiples_NV5',
'V-ExpressionsBooleennesMultiples_NV6',
'V-OperateursComparaison_NV1',
'V-OperateursComparaison_NV2',
'V-OperateursComparaison_NV3',
'V-OperateursLogiques_NV2',
'V-OperateursLogiques_NV3',
'V-OperateursLogiques_NV4',
'V-OperateursLogiques_NV5'
];

pg('target-profiles').insert({name: 'test uha', isPublic: true}).then(() => {
    pg('target-profiles').where({name: 'test uha', isPublic: true}).then(async (row) => {
        const idTargetProfiles = row[0].id;
        console.log(idTargetProfiles);
        for await(let iSkill of skills) {
            await pg('target-profiles_skills').insert({targetProfileId: idTargetProfiles, skillId: iSkill});
        }
    }).finally(() => {
        pg.destroy();
    });;
})
